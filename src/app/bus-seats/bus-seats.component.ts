import { BusService } from './../services/bus.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-bus-seats',
  templateUrl: './bus-seats.component.html',
  styleUrls: ['./bus-seats.component.css'],
})
export class BusSeatsComponent implements OnInit, OnDestroy {
  bus: any;

  bookTimeOut: any;

  selected: any;

  colorMap: any = {
    BLOCKED: '/assets/seat_gray.svg',
    AVAILABLE: '/assets/seat_cyan.svg',
    BOOKED: '/assets/seat_orange.svg',
    SELECTED: '/assets/seat_green.svg',
    'SOLD (M)': '/assets/seat_blue.svg',
    'SOLD (F)': '/assets/seat_red.svg',
  };

  form = new FormGroup({
    name: new FormControl(),
    email: new FormControl(),
    phone: new FormControl(),
    gender: new FormControl(),
  });

  showForm = false;

  seatCode(i: number, j: number) {
    var alpha = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    return `${alpha[i]}-${j + 1}`;
  }

  getSeatImage(row: number, col: number) {
    if (this.selected && this.selected.i === row && this.selected.j === col) {
      return this.colorMap.SELECTED;
    }
    return this.colorMap[this.bus?.seats[row][col]];
  }

  constructor(private service: BusService, private router: Router) {}

  ngOnInit(): void {
    this.bus = this.service.bus;
    if (!this.bus) this.router.navigate(['']);
  }

  ngOnDestroy() {
    clearTimeout(this.bookTimeOut);
  }

  onClick(i: number, j: number) {
    if (this.bus.seats[i][j] === 'AVAILABLE') {
      if (this.selected) {
        this.service.unBookSeat(this.selected.i, this.selected.j);
        clearTimeout(this.bookTimeOut);
        this.bookTimeOut = null;
      }

      let success = this.service.bookSeat(i, j)?.success;

      if (success) {
        this.selected = { i, j };
        this.bookTimeOut = setTimeout(() => {
          this.service.unBookSeat(i, j);
          this.selected = null;
          alert('Booked seat released after 10 minute timeout.');
        }, 10 * 60 * 1000);
      }
    } else if (this.selected.i === i && this.selected.j === j) {
      let success = this.service.unBookSeat(i, j)?.success;
      if (success) {
        this.service.unBookSeat(i, j);
        this.selected = null;
        clearTimeout(this.bookTimeOut);
        this.bookTimeOut = null;
      }
    }
  }

  toggleForm() {
    this.showForm = !this.showForm;
  }

  formClick($event: Event) {
    $event.stopPropagation();
  }

  onSubmit() {
    let response = this.service.purchaseTicket(
      this.selected.i,
      this.selected.j,
      this.form.value
    );

    if (response.success) {
      this.selected = null;
      this.toggleForm();
    } else alert(response.message);
  }
}
