import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FxtestComponent } from './fxtest.component';

describe('FxtestComponent', () => {
  let component: FxtestComponent;
  let fixture: ComponentFixture<FxtestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FxtestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FxtestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
