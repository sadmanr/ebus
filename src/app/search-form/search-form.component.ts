import { DataService } from './../services/data.service';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.css'],
})
export class SearchFormComponent implements OnInit {
  @Input() view = 'column';

  @Input() loading = false;

  @Input('form-value') formValue: any;

  @Output('on-search') onSearch = new EventEmitter();

  districts: { id: number; value: string }[] = [];

  form = new FormGroup({
    from: new FormControl(),
    to: new FormControl(),
    journeyDate: new FormControl(),
    arrivalDate: new FormControl(),
  });

  constructor() {
    this.districts = new DataService().getDistricts();
  }

  ngOnInit(): void {
    if (this.formValue) {
      this.form.setValue(this.formValue);
      this.onSubmit();
    }
  }

  onSubmit() {
    if (this.form.valid) {
      this.onSearch.emit(this.form.value);
    }
  }
}
