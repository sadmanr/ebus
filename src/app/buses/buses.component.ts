import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-buses',
  templateUrl: './buses.component.html',
  styleUrls: ['./buses.component.css'],
})
export class BusesComponent implements OnInit {
  @Input() buses: [];

  constructor() {
    this.buses = [];
  }

  ngOnInit(): void {}
}
