import { SearchDataService } from './../services/search-data.service';
import { BusService } from './../services/bus.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.css'],
})
export class SearchPageComponent implements OnInit, OnDestroy {
  searchData: any;

  subscription: Subscription = new Subscription();

  loading: boolean = false;

  buses: any = [];

  constructor(
    private route: ActivatedRoute,
    private service: BusService,
    private searchDataService: SearchDataService
  ) {}

  static isValidDate(d: any) {
    return d instanceof Date;
  }

  ngOnInit(): void {
    this.subscription = this.searchDataService.subject.subscribe((data) => {
      if (data) {
        let journeyDate = data?.journeyDate || '';
        let arrivalDate = data?.arrivalDate || '';

        this.searchData = {
          from: data?.from,
          to: data?.to,
          journeyDate: journeyDate ? new Date(journeyDate) : null,
          arrivalDate: arrivalDate ? new Date(arrivalDate) : null,
        };
        this.loading = data ? true : false;
      }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  searchBus($searchData: any) {
    this.loading = true;
    this.service.getBuses(1, 2).subscribe((response) => {
      this.buses = response;
      this.loading = false;
    });
  }
}
