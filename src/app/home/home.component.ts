import { SearchDataService } from './../services/search-data.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  constructor(private router: Router, private service: SearchDataService) {}

  ngOnInit(): void {}

  onSearch($searchData: any) {
    this.service.storeSearchData($searchData);
    //this.router.navigate(['/search'], { queryParams: $searchData });
    this.router.navigate(['/search']);
  }
}
