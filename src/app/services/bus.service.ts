import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class BusService {
  bus: any;
  apiUrl = 'https://sman2901.pythonanywhere.com/api';

  constructor(private http: HttpClient) {}

  bookSeat(i: number, j: number) {
    if (this.bus) {
      var seat = this.bus.seats[i][j];

      if (seat !== 'AVAILABLE')
        return { success: false, message: 'Seat is not available' };
      this.bus.seats[i][j] = 'BOOKED';
      return { success: true, message: 'Seat booked' };
    }
    return;
  }

  unBookSeat(i: number, j: number) {
    if (this.bus) {
      var seat = this.bus.seats[i][j];

      if (seat !== 'BOOKED')
        return { success: false, message: 'Seat is not booked' };
      this.bus.seats[i][j] = 'AVAILABLE';
      return { success: true, message: 'Seat released' };
    }
    return null;
  }

  purchaseTicket(i: number, j: number, data: any) {
    let adjacent = [1, 0, 3, 2];
    if (data?.gender === 'M' && this.bus.seats[i][adjacent[j]] === 'SOLD (F)') {
      return {
        success: false,
        message: 'Select a seat without adjacent female passender',
      };
    }

    this.bus.seats[i][j] = `SOLD (${data?.gender})`;
    return { success: true, message: 'Ticket purchased' };
  }

  getBuses(from: number | string, to: number | string) {
    var url = `${this.apiUrl}/trips/${from}/${to}`;
    return this.http.get(url);
  }
}
