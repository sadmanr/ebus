import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SearchDataService {
  subject: BehaviorSubject<any> = new BehaviorSubject(null);

  constructor() {}

  storeSearchData(data: any) {
    this.subject.next(data);
  }
}
