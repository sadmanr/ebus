import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-bus-seat',
  templateUrl: './bus-seat.component.html',
  styleUrls: ['./bus-seat.component.css'],
})
export class BusSeatComponent implements OnInit {
  @Input('img-url') imgUrl: any = '';

  @Output() click = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  onClick($event: Event) {
    $event.stopPropagation();
    this.click.emit();
  }
}
