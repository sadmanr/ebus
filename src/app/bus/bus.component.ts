import { BusService } from './../services/bus.service';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bus',
  templateUrl: './bus.component.html',
  styleUrls: ['./bus.component.css'],
})
export class BusComponent implements OnInit {
  @Input() bus: any;

  @Input('show-link') showLink = true;

  panelOpenState = false;

  countAvailable(seats: any) {
    var count = 0;
    for (var i in seats) {
      for (var j in seats[i]) {
        if (seats[i][j] === 'AVAILABLE') {
          count++;
        }
      }
    }
    return count;
  }

  constructor(private router: Router, private service: BusService) {}

  ngOnInit(): void {}

  onClick() {
    this.service.bus = this.bus;
    this.router.navigate(['/bus']);
  }
}
